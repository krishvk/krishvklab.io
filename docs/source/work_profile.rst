Work Profile
++++++++++++

Worked on processor verification of Synopsys’ ARC family of processors, for around 11+ years

RPGs
----

* With primary focus on :code:`Random Program Generators (RPG)` for processor level verification,
  *worked on* and *developed from scratch* a wide variety of RPGs
* Developed an offline generator **from scratch**, that generates code patterns at lightning speed
  without any communication with golden model
* Developed an online generator **from scratch**, that communicates with golden model to generate
  context aware programs
* Developed a Python environment **from scratch** to generate random Application Extensions to aid
  in verification
* Working on a C-based environment **from scratch** to aid in *high level language (HLL)* test
  development to focus on scenarios rather than low level details during test development
* Got into touch with most of the processor features, while supporting them in the :code:`RPGs`
* Worked on multiple MLVs, PLVs, ISA, Functional and Code coverage closures during early stages of
  career that helped me understand the requirements of verification in terms of RPGs
* Understood product design and management principles while designing the user interface and
  features taking regular feedback from around 50+ verification engineers using the tool from time
  to time
* Managed the :code:`RPGs` end to end from preparing Roadmaps to monitoring the product health
* Developed numerous solutions as part of RPGs to solve specific verification issues like Test-TB
  communication, inter core synchronization, memory management in random environments etc. that are
  both scalable and reusable

Best Practices
--------------

* Leading the Verification Best Practices team, that monitors and solves critical issues in the
  verification to improve the verification efficiency
* Worked on standardizing the methodologies used across the team
* Proactively solved issues faced in verification by collaborating with different teams
* Worked on bringing up a centralized dashboard to monitor the health of entire product range

AI-ML
-----

* Developed a log analyzer tool as part of Master's thesis that automates the failure logs triaging
  saving a lot of verification cycles

Miscellaneous
-------------

* Worked directly on MLV & PLV of modules like MMU, AP, Cache Coherency and processor debug features
* Developed and maintained Co-Simulation environment for PLV
* Learned and deployed many product-management tools like :code:`grafana`, :code:`gitlab's CI` and
  documentation tools like :code:`sphinx`
* Developed standards for managing ISA information inside the organization that offers flexibility
  and scalability
* Good with automation, developed and maintained several flows in verification
