Personal Profile
++++++++++++++++

A software enthusiast and programmer, with 11+ years of relevant experience. My primary area of work
is **Random Program Generators** for processor verification. More areas I have worked on are as
follows


Skillset
--------

.. csv-table::
    :delim: ,
    :widths: 8, 56

    **Languages**, :code:`SystemVerilog` :code:`Python` :code:`Assembly` :code:`Perl` :code:`tcsh` :code:`Makefile` :code:`C++` :code:`C` :code:`SQLite` :code:`MySQL` :code:`zsh` :code:`bash` :code:`Verilog` :code:`E (Specman)`
    **Areas Worked**, :code:`Random Program Generators` :code:`Processor Verification` :code:`Co-Simulation` :code:`Automation` :code:`Databases` :code:`DITA` :code:`Language Servers` :code:`UVM` :code:`Compilers`
    **Processor Features**, :code:`ISA` :code:`Multicore Systems` :code:`Caches (L1 & L2)` :code:`Cache coherency` :code:`MMU` :code:`MPU` :code:`Debug Features` :code:`VLIW` :code:`SIMD` :code:`Multi issue pipes` :code:`IPXACT`
    **Libraries**, :code:`pygraphviz` :code:`graphviz` :code:`scikit-learn` :code:`Pygls` :code:`SLY` :code:`Matplotlib` :code:`Pandas` :code:`NumPy` :code:`Flask` :code:`Plotly` :code:`Dash`
    **Tools & Software**, :code:`VSCode` :code:`vim` :code:`perforce` :code:`git` :code:`Sphinx` :code:`Doxygen` :code:`Grafana` :code:`gitlab` :code:`JIRA` :code:`JAMA` :code:`DVE` :code:`Verdi` :code:`svn`


Education
---------

Being a self and continuous learner, I enjoy acquiring new knowledge all the time. Below are the
**structured courses** I have perceived apart from the above skills I have acquired through on the
fly learning.

.. csv-table::
    :header: Date, Degree, Title, Institute

    Mar 2022, M.Tech, Data Science & Engineering, BITS Pilani
    Jan 2022, Course, `Interactive python dashboards | Plotly Dash <https://www.udemy.com/certificate/UC-d0b89b8b-d009-4e5e-85f4-a6e63ccb212b/>`__, Udemy
    Oct 2019, Course, `Machine Learning Foundations: A Case Study Approach <https://www.coursera.org/account/accomplishments/certificate/EN3XMJ4Y9KPC>`__, Coursera (University of Washington)
    Jan 2019, Course, `Introduction to Data Science in Python <https://www.coursera.org/account/accomplishments/verify/LEDUPG84AFH3>`__, Coursera (University of Michigan)
    Nov 2010, Diploma, VLSI Front End (On Job Training), VEDA IIT
    July 2010, B.Tech, Electronics and Communications Engineering, SVIT (JNTU)
    May 2006, Intermediate, MPC, APRJC Nagarjuna Sagar

* A discontinued **Master of Science in VLSI** program at IIIT-H
* Few uncertified courses on Coursera and other platforms


Publications
------------

#. `Kasula, V. K., & Chedella, S. S. (2016)` **Barrier Insertion in Test Programs for Controlled Verification of Cache Coherency** Synopsys India Technical Conference (SITC). Bangalore.
#. `Kasula, Vijaya Krishna. (2016)` **Reverse Generation Technique for Functional Verification of Processor Cores** Synopsys India Technical Conference (SITC). Bangalore.
#. `Kasula, Vijaya Krishna; Tadiboina, Gopi Krishna. (2016)` **Test Generation for Processors with Extended Address Capability** Synopsys India Technical Conference (SITC). Bangalore.


OSS
---

Developed an opensource library in Python `txtoflow <https://pypi.org/project/txtoflow/>`__ which
can **generate flow chart from pseudo-code** written in C-like languages.


Coding Competitions
-------------------

Occasionally, I participate in online coding competitions and managed to secure a certificate of
recommendation from **HARVARD University, Cambridge, MA, USA**, acknowledging my performance in an
online coding competition held in collaboration with **NASA** on **TopCoder**


Online Presence
---------------

* `Stackoverflow <https://stackoverflow.com/users/1838076/krishna>`__
* `LinkedIn <https://www.linkedin.com/in/krishvk>`__
* `Gitlab <https://gitlab.com/krishvk>`__
* `Github <https://github.com/krishvk>`__

..
    Hide as Github is not active
    If they are viewing this, they are on the only source that is Gitlab
    * About me on `Gitlab <https://krishvk.gitlab.io>`__ and `Github <https://krishvk.github.io>`__
